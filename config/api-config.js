var express = require("express");
var app = express();
var path  = require('path');
const mysql = require('mysql');
const jwt = require('jsonwebtoken');
var db = require('./database');
var dbfunc = require('./db-function');
var http  = require('http')
var bodyParser = require('body-parser');
var ProgramRoute = require('../app/routes/program.route');
var ProgramTypeRoute = require('../app/routes/program-type.route');

var errorCode = require('../common/error-code')
var errorMessage = require('../common/error-methods')
var checkToken = require('./secureRoute');


dbfunc.connectionCheck.then((data) =>{
 }).catch((err) => {
     console.log(err);
 });

 app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.use(bodyParser.json());



var programApi = express.Router();

app.use(express.static(path.join(__dirname, 'public')));
//body parser middleware

app.use('/program',programApi);


app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});


// index route
app.get('/', (req,res) => {
    res.send('hello world');
});

var ApiConfig = {
  app: app
}

ProgramRoute.init(programApi);

var programTypeApi =express.Router();
app.use('/programType',programTypeApi);
ProgramTypeRoute.init(programTypeApi)


module.exports = ApiConfig;
