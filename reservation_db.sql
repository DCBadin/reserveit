 
DROP TABLE IF EXISTS `feedback`;
DROP TABLE IF EXISTS `reservations`;
DROP TABLE IF EXISTS `terms`;
DROP TABLE IF EXISTS `contact`;
DROP TABLE IF EXISTS `service`;
DROP TABLE IF EXISTS `customer`;
DROP TABLE IF EXISTS `auth`;
DROP TABLE IF EXISTS `user`;
DROP TABLE IF EXISTS `program_service`;
DROP TABLE IF EXISTS `program`;
DROP TABLE IF EXISTS `service_type`;
DROP TABLE IF EXISTS `roles`;
DROP TABLE IF EXISTS `user_type`;
DROP TABLE IF EXISTS `program_type`;



-- ************************************** `service_type`

CREATE TABLE `service_type`
(
 `id`   INT NOT NULL AUTO_INCREMENT ,
 `name` VARCHAR(45) NOT NULL ,

PRIMARY KEY (`id`)
);






-- ************************************** `roles`

CREATE TABLE `roles`
(
 `id`   INT NOT NULL AUTO_INCREMENT ,
 `name` VARCHAR(45) NOT NULL ,

PRIMARY KEY (`id`)
);






-- ************************************** `user_type`

CREATE TABLE `user_type`
(
 `id`   INT NOT NULL AUTO_INCREMENT ,
 `name` VARCHAR(45) NOT NULL ,

PRIMARY KEY (`id`)
);






-- ************************************** `program_type`

CREATE TABLE `program_type`
(
 `id`   INT NOT NULL AUTO_INCREMENT ,
 `name` VARCHAR(45) NOT NULL ,

PRIMARY KEY (`id`)
);






-- ************************************** `program_service`

CREATE TABLE `program_service`
(
 `program_type_id` INT NOT NULL ,
 `service_type_id` INT NOT NULL ,

PRIMARY KEY (`program_type_id`, `service_type_id`),
KEY `fkIdx_121` (`program_type_id`),
CONSTRAINT `FK_121` FOREIGN KEY `fkIdx_121` (`program_type_id`) REFERENCES `program_type` (`id`),
KEY `fkIdx_124` (`service_type_id`),
CONSTRAINT `FK_124` FOREIGN KEY `fkIdx_124` (`service_type_id`) REFERENCES `service_type` (`id`)
);






-- ************************************** `program`

CREATE TABLE `program`
(
 `id`              INT NOT NULL AUTO_INCREMENT ,
 `name`            VARCHAR(45) NOT NULL ,
 `program_type_ID` INT NOT NULL ,

PRIMARY KEY (`id`),
KEY `fkIdx_46` (`program_type_ID`),
CONSTRAINT `FK_46` FOREIGN KEY `fkIdx_46` (`program_type_ID`) REFERENCES `program_type` (`id`)
);






-- ************************************** `user`

CREATE TABLE `user`
(
 `id`            INT NOT NULL AUTO_INCREMENT ,
 `program_id`    INT NOT NULL ,
 `user_type_id`  INT NOT NULL ,
 `name`          VARCHAR(45) NOT NULL ,
 `email`         VARCHAR(45) NOT NULL ,
 `mobile_number` VARCHAR(45) NOT NULL ,
 `date_created`  DATETIME NOT NULL ,
 `date_updated`  DATETIME NOT NULL ,
 `parent_id`     INT NOT NULL ,

PRIMARY KEY (`id`),
KEY `fkIdx_49` (`program_id`),
CONSTRAINT `FK_49` FOREIGN KEY `fkIdx_49` (`program_id`) REFERENCES `program` (`id`),
KEY `fkIdx_56` (`user_type_id`),
CONSTRAINT `FK_56` FOREIGN KEY `fkIdx_56` (`user_type_id`) REFERENCES `user_type` (`id`),
KEY `fkIdx_154` (`parent_id`),
CONSTRAINT `FK_154` FOREIGN KEY `fkIdx_154` (`parent_id`) REFERENCES `user` (`id`)
);






-- ************************************** `terms`

CREATE TABLE `terms`
(
 `id`      INT NOT NULL AUTO_INCREMENT ,
 `start`   DATETIME NOT NULL ,
 `end`     DATETIME NOT NULL ,
 `user_id` INT NOT NULL ,

PRIMARY KEY (`id`),
KEY `fkIdx_142` (`user_id`),
CONSTRAINT `FK_142` FOREIGN KEY `fkIdx_142` (`user_id`) REFERENCES `user` (`id`)
);






-- ************************************** `contact`

CREATE TABLE `contact`
(
 `id`      INT NOT NULL AUTO_INCREMENT ,
 `city`    VARCHAR(45) NOT NULL ,
 `data`    VARCHAR(45) NOT NULL ,
 `user_id` INT NOT NULL ,

PRIMARY KEY (`id`),
KEY `fkIdx_118` (`user_id`),
CONSTRAINT `FK_118` FOREIGN KEY `fkIdx_118` (`user_id`) REFERENCES `user` (`id`)
);






-- ************************************** `service`

CREATE TABLE `service`
(
 `user_id`         INT NOT NULL ,
 `role_id`         INT NOT NULL ,
 `service_type_id` INT NOT NULL ,

PRIMARY KEY (`user_id`),
KEY `fkIdx_90` (`user_id`),
CONSTRAINT `FK_90` FOREIGN KEY `fkIdx_90` (`user_id`) REFERENCES `user` (`id`),
KEY `fkIdx_103` (`role_id`),
CONSTRAINT `FK_103` FOREIGN KEY `fkIdx_103` (`role_id`) REFERENCES `roles` (`id`),
KEY `fkIdx_110` (`service_type_id`),
CONSTRAINT `FK_110` FOREIGN KEY `fkIdx_110` (`service_type_id`) REFERENCES `service_type` (`id`)
);






-- ************************************** `customer`

CREATE TABLE `customer`
(
 `dob`        DATE NOT NULL ,
 `first_name` VARCHAR(45) NOT NULL ,
 `last_name`  VARCHAR(45) NOT NULL ,
 `user_id`    INT NOT NULL ,

PRIMARY KEY (`user_id`),
KEY `fkIdx_86` (`user_id`),
CONSTRAINT `FK_86` FOREIGN KEY `fkIdx_86` (`user_id`) REFERENCES `user` (`id`)
);






-- ************************************** `auth`

CREATE TABLE `auth`
(
 `id`           INT NOT NULL AUTO_INCREMENT ,
 `username`     VARCHAR(45) NOT NULL ,
 `password`     VARCHAR(45) NOT NULL ,
 `fl_active`    VARCHAR(45) NOT NULL ,
 `date_created` DATETIME NOT NULL ,
 `date_updated` DATETIME NOT NULL ,
 `user_id`      INT NOT NULL ,

PRIMARY KEY (`id`),
KEY `fkIdx_68` (`user_id`),
CONSTRAINT `FK_68` FOREIGN KEY `fkIdx_68` (`user_id`) REFERENCES `user` (`id`)
);






-- ************************************** `reservations`

CREATE TABLE `reservations`
(
 `id`      INT NOT NULL AUTO_INCREMENT ,
 `client`  INT NOT NULL ,
 `term_id` INT NOT NULL ,

PRIMARY KEY (`id`),
KEY `fkIdx_131` (`client`),
CONSTRAINT `FK_131` FOREIGN KEY `fkIdx_131` (`client`) REFERENCES `user` (`id`),
KEY `fkIdx_145` (`term_id`),
CONSTRAINT `FK_145` FOREIGN KEY `fkIdx_145` (`term_id`) REFERENCES `terms` (`id`)
);






-- ************************************** `feedback`

CREATE TABLE `feedback`
(
 `id`             INT NOT NULL AUTO_INCREMENT ,
 `value`          INT NOT NULL ,
 `comment`        VARCHAR(45) NOT NULL ,
 `from`           INT NOT NULL ,
 `to`             INT NOT NULL ,
 `reservation_id` INT NOT NULL ,

PRIMARY KEY (`id`),
KEY `fkIdx_157` (`from`),
CONSTRAINT `FK_157` FOREIGN KEY `fkIdx_157` (`from`) REFERENCES `user` (`id`),
KEY `fkIdx_160` (`to`),
CONSTRAINT `FK_160` FOREIGN KEY `fkIdx_160` (`to`) REFERENCES `user` (`id`),
KEY `fkIdx_163` (`reservation_id`),
CONSTRAINT `FK_163` FOREIGN KEY `fkIdx_163` (`reservation_id`) REFERENCES `reservations` (`id`)
);





