var programTypeModel = require("../models/program-type-model.js");


var programTypeService = {
    createProgramType: createProgramType
}

function createProgramType(programTypeData) {
    return new Promise((resolve,reject) => {
        programTypeModel.createProgramType(programTypeData).then((data)=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    })

}

module.exports = programTypeService;
