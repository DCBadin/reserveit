var programModel = require("../models/program-model.js");


var programService = {
    createProgram: createProgram
}

function createProgram(programData) {
    return new Promise((resolve,reject) => {
        programModel.createProgram(programData).then((data)=>{
            resolve(data);
        }).catch((err) => {
            reject(err);
        })
    })

}

module.exports = programService;
