const programTypeService = require('../services/program-type.service');
var schema = require('../schema/programTypeValidationSchema.json')
var iValidator = require('../../common/iValidator');
var errorCode = require('../../common/error-code');
var errorMessage = require('../../common/error-methods');
var mail = require('./../../common/mailer.js');


function init(router) {
    router.route('/')
        .post(createProgramType)
}


function createProgramType(req,res) {
  var programTypeData=req.body;

  //Validating the input entity
   var json_format = iValidator.json_schema(schema.postSchema, programTypeData, "programType");
   if (json_format.valid == false) {
     return res.status(422).send(json_format.errorMessage);
   }

  programTypeService.createProgramType(programTypeData).then((data) => {
    res.json(data);
  }).catch((err) => {
    mail.mail(err);
    res.json(err);
  });

}




module.exports.init = init;
