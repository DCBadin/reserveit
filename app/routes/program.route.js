const programService = require('../services/program.service');
var schema = require('../schema/programValidationSchema.json')
var iValidator = require('../../common/iValidator');
var errorCode = require('../../common/error-code');
var errorMessage = require('../../common/error-methods');
var mail = require('./../../common/mailer.js');


function init(router) {
    router.route('/')
        .post(createProgram)
        .get(getProgram);
}


function createProgram(req,res) {
  var programData=req.body;

  //Validating the input entity
   var json_format = iValidator.json_schema(schema.postSchema, programData, "program");
   if (json_format.valid == false) {
     return res.status(422).send(json_format.errorMessage);
   }

  programService.createProgram(programData).then((data) => {
    res.json(data);
  }).catch((err) => {
    mail.mail(err);
    res.json(err);
  });

}

function getProgram(req,res) {
  var programData=req.body;

  //Validating the input entity
   var json_format = iValidator.json_schema(schema.postSchema, programData, "program");
   if (json_format.valid == false) {
     return res.status(422).send(json_format.errorMessage);
   }

  programService.createProgram(programData).then((data) => {
    res.json(data);
  }).catch((err) => {
    mail.mail(err);
    res.json(err);
  });

}


module.exports.init = init;
